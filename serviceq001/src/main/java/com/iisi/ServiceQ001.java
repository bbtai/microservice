package com.iisi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Chien-Tai Huang on 2018/6/18.
 */
@SpringBootApplication
@EnableEurekaClient
@RestController
public class ServiceQ001 {

	public static void main(String[] args) {
		SpringApplication.run(ServiceQ001.class, args);
	}

	@Value("${server.port}")
	String port;

	@RequestMapping("/Q001")
	public String home(@RequestParam String ban) {
		if (ban.equalsIgnoreCase("28859051")) {
			return "營業人統一編號： " + ban + "<br/>" + "營業狀況：營業中" + "<br/>" + "負責人姓名：何寶中" + "<br/>" + "營業人名稱：資拓宏宇國際股份有限公司"
					+ "<br/>" + "營業（稅籍）登記地址：新北市板橋區新民里縣民大道２段７號６樓" + "<br/>" + "資本額(元)：700,348,210" + "<br/>"
					+ "組織種類：股份有限公司( 1 )" + "<br/>" + "設立日期：0970417" + "<br/>"
					+ "登記營業項目：未分類其他資訊服務( 639099 ), 人力供應( 782000 ), 電腦套裝軟體批發( 464112 )" + "<br/>" + "查詢埠號：" + port;
		} else if (ban.equalsIgnoreCase("27950876")) {
			return "營業人統一編號： " + ban + "<br/>" + "營業狀況：營業中" + "<br/>" + "負責人姓名：黃秀谷" + "<br/>" + "營業人名稱：中華電信股份有限公司企業客戶分公司"
					+ "<br/>" + "營業（稅籍）登記地址：臺北市大安區住安里信義路４段８８號１６樓" + "<br/>" + "資本額(元)：0" + "<br/>"
					+ "組織種類：本國公司設立之分公司( 9 )" + "<br/>" + "設立日期：0960109" + "<br/>"
					+ "登記營業項目：衛星電視頻道經營( 602012 ),電路工程( 433112 ), 區域網路及寬頻網路設備製造( 272915 ) " + "<br/>" + "查詢埠號：" + port;
		} else {
			return "營業人統一編號： " + ban + "，查無資料。"+ "<br/>" + "查詢埠號：" + port;
		}
	}

}
