package com.iisi.service;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Chien-Tai Huang on 2018/6/18.
 */
@Service
public class Q001Service {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "serviceQ001Error")
    public String serviceQ001(String ban) {
        return restTemplate.getForObject("http://ServiceQ001/Q001?ban="+ban,String.class);
    }
    
    public String serviceQ001Error(String ban) {
        return "System error，cant query "+ban+".";
    }

}
