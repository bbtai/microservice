# Micro-Service Test 04_1

## eureka-server

### Samplle of microservice registry, discovery and consume.
---
#### Archiecture of Samples.

> portal(7001~7002) <=> service(6000~6002) <=> eureka(5000~5002) 

---

#### 1.Run eureka-server
You can maven install and then run the samples：
> Standalone：java -jar eureka-server-0.0.1-SNAPSHOT.jar 
>  
> Cluster server1：java -jar eureka-server-0.0.1-SNAPSHOT.jar --spring.profiles.active=server1  
> 
> Cluster server2：java -jar eureka-server-0.0.1-SNAPSHOT.jar --spring.profiles.active=server2 

Cluster need to add host mapping to .host file (windows)：  
> 127.0.0.1 server1  
> 127.0.0.1 server2

You can access the eureka-server console：
> http://localhost:5000  
> http://server1:5001  
> http://server2:5002



---
#### 2.Run service
You can maven install and then run the service：
> instance 0：java -jar service-q001-0.0.1-SNAPSHOT.jar  
> 
> instance 1：java -jar service-q001-0.0.1-SNAPSHOT.jar --spring.profiles.active=service1  
> 
> instance 2：java -jar service-q001-0.0.1-SNAPSHOT.jar --spring.profiles.active=service2

Instance 0 only registry to standalone eureka standalone.Instance 1, 2 registry to eureka standalone, cluster1, cluster2.

You can test service with url：
> http://localhost:6000/Q001?ban=28859051  
> http://localhost:6001/Q001?ban=28859051  
> http://localhost:6002/Q001?ban=28859051

---
#### 3.Run portal with Ribbon&Hystrix&Feign
You can maven install and then run the portal：
> Ribbon：java -jar portal-0.0.1-SNAPSHOT.jar  
> Hystrix：java -jar portalwithCB-0.0.1-SNAPSHOT.jar  
> Feign：java -jar portalwithFeign-0.0.1-SNAPSHOT.jar

You can test portal with url：
> Ribbon：http://localhost:7001/Q001?ban=28859051  
> Hystrix：http://localhost:7002/Q001?ban=28859051  
> Feign：http://localhost:7003/Q001?ban=28859051 

2018/07/10 Modified by cti.
