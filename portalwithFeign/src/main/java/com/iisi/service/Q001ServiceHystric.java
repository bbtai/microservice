package com.iisi.service;

import org.springframework.stereotype.Component;

/**
 * Created by Chien-Tai Huang on 2018/6/18.
 */
@Component
public class Q001ServiceHystric implements Q001Service {
	
    @Override
    public String serviceQ001(String ban) {
        return "System error，cant query "+ban+".";
    }
    
    /*public String serviceQ001Error(String ban) {
        return "System error，cant query "+ban+".";
    }*/
}
