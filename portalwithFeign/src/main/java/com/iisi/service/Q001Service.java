package com.iisi.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Chien-Tai Huang on 2018/6/18.
 */
@FeignClient(value = "ServiceQ001",fallback = Q001ServiceHystric.class)
public interface Q001Service {
    @RequestMapping(value = "/Q001",method = RequestMethod.GET)
    String serviceQ001(@RequestParam(value = "ban") String ban);
}
