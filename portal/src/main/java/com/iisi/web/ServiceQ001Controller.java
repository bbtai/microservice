package com.iisi.web;

import com.iisi.service.Q001Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Chien-Tai Huang on 2018/6/18.
 */
@RestController
public class ServiceQ001Controller {



    @Autowired
    Q001Service q001Service;

    @RequestMapping(value = "/Q001")
    public String q001Service(@RequestParam String ban){
        return q001Service.serviceQ001(ban);
    }


}
